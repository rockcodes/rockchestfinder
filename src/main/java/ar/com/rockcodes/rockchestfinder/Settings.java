package ar.com.rockcodes.rockchestfinder;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;



public class Settings {

	
	public static Boolean debug = false;
	public static List<ItemStack> items = new ArrayList<ItemStack>();
	
	public static void loadSettings(){
		FileConfiguration config = ChestFinder.plugin.getConfig() ;
		
		Settings.debug = config.getBoolean("debug",true);
		Settings.items = (List<ItemStack>) config.getList("items",new ArrayList<ItemStack>());
		
	}
	
	public static void reload(){
		ChestFinder.plugin.reloadConfig();
		Settings.loadSettings();
	}
	
	public static void saveSettings(){
		FileConfiguration config = ChestFinder.plugin.getConfig() ;
		config.set("debug", Settings.debug);
		config.set("items", Settings.items);
		ChestFinder.plugin.saveConfig();
	}
	
	
}
