package ar.com.rockcodes.rockchestfinder.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Msg {

	private static String tag = ChatColor.GREEN+"["+ChatColor.YELLOW+"CHESTCHECK"+ChatColor.GREEN+"]"+ChatColor.RESET+"- ";
	
	public static void sendMsg(Player player, String str){
		player.sendMessage(tag+str);
	}

	public static void sendMsg(CommandSender sender, String str) {
		sender.sendMessage(tag+str);
		
	}
}
