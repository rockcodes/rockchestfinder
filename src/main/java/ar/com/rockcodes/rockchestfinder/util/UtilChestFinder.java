package ar.com.rockcodes.rockchestfinder.util;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import ar.com.rockcodes.rockchestfinder.ChestFinder;

public class UtilChestFinder {

	
	static public YamlConfiguration loadconfig(String filename){
		final File challengesfile = new File(ChestFinder.plugin.getDataFolder(), filename);
		YamlConfiguration chconfig = new YamlConfiguration();
		if (!challengesfile.exists()) {
			try {
				chconfig.save(challengesfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			try {
				chconfig.load(challengesfile);
			
			} catch (IOException | InvalidConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return chconfig;
	}
	
	static public void saveconfig(YamlConfiguration config, String filename){
		try {
			config.save(new File(ChestFinder.plugin.getDataFolder(), filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
