package ar.com.rockcodes.rockchestfinder;

import java.io.DataInputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.minecraft.server.v1_8_R3.NBTCompressedStreamTools;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import ar.com.rockcodes.rockchestfinder.util.Msg;
import ar.com.rockcodes.rockchestfinder.util.UtilChestFinder;


public class FinderTask implements Runnable {

	int msgid = 0;
	float porcentaje = 0;
	boolean finished = false;
	World world ;
	boolean cancelled = false;
	List<Location> chestlist;
	BukkitTask btask;
	CommandSender sender;
	
	public FinderTask (World w, CommandSender sender){
		this.world = w;
		this.chestlist = new ArrayList<Location>();
		this.sender = sender;
	}
	
	public List<Location> getChestList(){
		return this.chestlist;
	}
	public boolean isFinished() {
		return finished;
	}
	
	

	
	@Override
	public void run() {
		//loadAllChunks(Bukkit.getServer().getWorlds().get(0));
		  Msg.sendMsg(sender,"FinderChest Task in "+world.getName()+" was started");

		btask = Bukkit.getScheduler().runTaskTimerAsynchronously(ChestFinder.plugin, new Runnable(){

			@Override
			public void run() {
				  Msg.sendMsg(sender,"ChestFinderTask in "+world.getName()+" "+FinderTask.this.porcentaje+"%");
				
			     if(FinderTask.this.porcentaje>=100 || FinderTask.this.isFinished()){
			       Msg.sendMsg(sender, "ChestFinderTask in "+world.getName()+" Finished! ");
			    	 FinderTask.this.btask.cancel();
				
			     }
			}
			
			
			
		} ,10L,40L );
		
		this.finished = false;
		List<Location> locs = findInChest(this.world);
		this.finished = true;
		
		
		if(locs == null) return;
		
		final File cfgfile = new File(ChestFinder.plugin.getDataFolder(), world.getName()+".yml");
		YamlConfiguration cfg = new YamlConfiguration();

			try {
				cfg.save(cfgfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		int i = 0;
		for(Location l : locs){
			
			cfg.set("C"+i, l.getBlockX()+","+l.getBlockY()+"," +l.getBlockZ());
			
			i++;

		}
		
		UtilChestFinder.saveconfig(cfg, world.getName()+".yml");
		
		

	}


	
	private List<Location> findInChest(World world){
		this.porcentaje = 0;
		 List<Location> loc = new ArrayList<Location>();
		 
        final Pattern regionPattern = Pattern.compile("r\\.([0-9-]+)\\.([0-9-]+)\\.mca");
        
        File worldDir = world.getWorldFolder().getAbsoluteFile();
        File regionDir = new File(worldDir, "region");
 
        File[] regionFiles = regionDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return regionPattern.matcher(name).matches();
            }
        });
 
        //Bukkit.getServer().getLogger().info("Found " + (regionFiles.length * 1024) + " chunk candidates in " + regionFiles.length + " files to check for loading ...");
        int loadedCount = 0;
        for (File f : regionFiles) {
        	
        
        if(this.cancelled) return null;
		 
		 RegionFile regionfile = new RegionFile(f);
		 
         Matcher matcher = regionPattern.matcher(f.getName());
         if (!matcher.matches()) {
         	Bukkit.getServer().getLogger().warning("FilenameFilter accepted unmatched filename: " + f.getName());
             continue;
         }

      

         for (int cx = 0; cx < 32; cx++) 
             for (int cz = 0; cz < 32; cz++) {
            	 
            	 if(this.cancelled) return null;
            	 if(!regionfile.hasChunk(cx,cz))continue;
            	 
             
            	 DataInputStream chunkdata = regionfile.getChunkDataInputStream(cx, cz);					 
					try {
						NBTTagCompound result = NBTCompressedStreamTools.a(chunkdata);
												
						NBTTagList tEn = result.getCompound("Level").getList("TileEntities",10);
						
						
						
						 NBTTagCompound[] tileEntities = new NBTTagCompound[tEn.size()];
						 
						for(int i = 0 ; i < tileEntities.length;i++){
							NBTTagCompound secc =(NBTTagCompound) tEn.get(i);
							tileEntities[i] = secc;
						}
						
						
						for(NBTTagCompound tentity : tileEntities){
						
							String id = tentity.getString("id");
							int x = tentity.getInt("x");
							int y = tentity.getInt("y");
							int z = tentity.getInt("z");
							if(!id.equals("Chest"))continue;
							
							NBTTagList itemlist = tentity.getList("Items",10);
							
							
							 NBTTagCompound[] items = new NBTTagCompound[itemlist.size()];
							 
							for(int i = 0 ; i < items.length;i++){
								NBTTagCompound secc =(NBTTagCompound) itemlist.get(i);
								items[i] = secc;
							}
							
							for(NBTTagCompound item :items){
								
								
								String itemid = item.getString("id");
								
								Material material = Bukkit.getUnsafe().getMaterialFromInternalName(itemid);
								for(ItemStack it : Settings.items){
									
									if(it.getType().equals(material)){
										loc.add(new Location(world,x,y,z));
										break;
									}
								}
							
							}
							
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
             }
         
         loadedCount++;
         this.porcentaje = (loadedCount*100) /regionFiles.length ;
    
        }
        
        return loc;
		
	}
	
	private List<Location> findBlock(World world){
		this.porcentaje = 0;
		 List<Location> loc = new ArrayList<Location>();
		// File f = new File(new File(wfolder, "region"), "r.0.0.mca");
		 
		 
		 
        final Pattern regionPattern = Pattern.compile("r\\.([0-9-]+)\\.([0-9-]+)\\.mca");
        
        File worldDir = world.getWorldFolder().getAbsoluteFile();
        File regionDir = new File(worldDir, "region");
 
        File[] regionFiles = regionDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return regionPattern.matcher(name).matches();
            }
        });
 
        //Bukkit.getServer().getLogger().info("Found " + (regionFiles.length * 1024) + " chunk candidates in " + regionFiles.length + " files to check for loading ...");
        int loadedCount = 0;
        for (File f : regionFiles) {
        	
        
		 
		 RegionFile regionfile = new RegionFile(f);
		 
         Matcher matcher = regionPattern.matcher(f.getName());
         if (!matcher.matches()) {
         	Bukkit.getServer().getLogger().warning("FilenameFilter accepted unmatched filename: " + f.getName());
             continue;
         }

         int mcaX = Integer.parseInt(matcher.group(1));
         int mcaZ = Integer.parseInt(matcher.group(2));

      

         for (int cx = 0; cx < 32; cx++) 
             for (int cz = 0; cz < 32; cz++) {
            	 
            	 if(!regionfile.hasChunk(cx,cz))continue;
            	 
             
            	 DataInputStream chunkdata = regionfile.getChunkDataInputStream(cx, cz);					 
					try {
						NBTTagCompound result = NBTCompressedStreamTools.a(chunkdata);
						
						NBTTagList seccions = result.getCompound("Level").getList("Sections",10);
						
						 NBTTagCompound[] slist = new NBTTagCompound[seccions.size()];
						 
						for(int i = 0 ; i < slist.length;i++){
							NBTTagCompound secc =(NBTTagCompound) seccions.get(i);
							slist[i] = secc;
						}
						
						
						int ccx = ((mcaX << 5) + cx )*16 ;
						int ccz = ((mcaZ << 5) + cz )*16 ;	
						
						for(NBTTagCompound secc : slist){
						
							Byte yY = secc.getByte("Y");
							
							int ccyd = yY.intValue();
							int ccy =  yY.intValue()*16;
							
							
							
							byte[] blockId = secc.getByteArray("Blocks");
							
						
							
							short[] blocks = new short[blockId.length];
							byte[] addId = new byte[0];
							//int[]blocksid = new int[blocks.length];
					        for (int index = 0; index < blockId.length; index++) {
					            if ((index >> 1) >= addId.length) { // No corresponding AddBlocks index
					                blocks[index] = (short) (blockId[index] & 0xFF);
					            } else {
					                if ((index & 1) == 0) {
					                    blocks[index] = (short) (((addId[index >> 1] & 0x0F) << 8) + (blockId[index] & 0xFF));
					                } else {
					                    blocks[index] = (short) (((addId[index >> 1] & 0xF0) << 4) + (blockId[index] & 0xFF));
					                }
					            }
					        }
					        
					        
					       /* int jj = 0;
					        for(short b : blocks){
					        	//System.out.println(Material.getMaterial(b).name()+"["+jj+"]");
					        	jj++;
					        }*/
							
					        
					        for(int iy = 0 ; iy<16;iy++)
					        	for(int iz = 0 ; iz<16;iz++)
					        		for(int ix = 0 ; ix<16;ix++)
					        		{
					        			short b = blocks[16*iz+ix+256*iy];
					        			
					        			if(Material.CHEST.equals(Material.getMaterial(b))){
					        				
											//System.out.println("CHEST ENCONTRADO EN X="+(ccx+ix)+" Y="+((ccy*16)+iy)+" Z="+(ccz+iz));
											loc.add(new Location(world,ix+ccx,(ccy+iy),iz+ccz));
					        			}
					        		}
									
							
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
             }
         
         loadedCount++;
         this.porcentaje = (loadedCount*100) /regionFiles.length ;
    
        }
        
        return loc;
		
	}

	public void cancel() {
		this.cancelled = true;
	}
     
    
	
	

}
