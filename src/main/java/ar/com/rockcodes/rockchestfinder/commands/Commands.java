package ar.com.rockcodes.rockchestfinder.commands;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ar.com.rockcodes.rockchestfinder.ChestFinder;
import ar.com.rockcodes.rockchestfinder.FinderTask;
import ar.com.rockcodes.rockchestfinder.Settings;
import ar.com.rockcodes.rockchestfinder.util.Msg;



public class Commands implements CommandExecutor{

	
	String world = "world";
	FinderTask task ;
	
	@Override
	public boolean onCommand(CommandSender sender, Command command,String label, String[] args) {
		
		
		
		if (sender instanceof Player) {
			Player player = (Player)sender;
			if(!player.isOp()){
				Msg.sendMsg(player, "Only OP can run this command");
				return true;
			}
		}
		if(args.length==0){command_help(sender,args); return true;}
			
		switch(args[0]){
			case "reload": command_reload(sender,args); return true;
			case "items": command_items(sender,args); return true;
			case "scan": command_scanchest(sender,args); return true;
			case "cancel": command_cancel(sender,args); return true;
			case "next": command_next(sender,args); return true;
			case "help": command_help(sender,args); return true;	
			default: command_help(sender,args); return true;
		}

	}



	private void command_items(CommandSender sender, String[] args) {
		if(args.length<2){ commands_items_help( sender,  args); return;}
		switch(args[1]){
			case "add": commands_items_add(sender,args); return ;
			case "del": commands_items_del(sender,args); return ;
			case "list": commands_items_list(sender,args); return;	
			case "help": commands_items_help(sender,args); return;	
			default: commands_items_help(sender,args); return;
		}
		
	}
	

	private void commands_items_list(CommandSender sender, String[] args) {
		
		String msg = "";
		int i = 0;
		for(ItemStack item : Settings.items){
			msg += " ["+i+"]"+item.getType().name() ;
			i++;
		}
		Msg.sendMsg(sender,"Item List:");
		Msg.sendMsg(sender,msg);
	}



	private void commands_items_del(CommandSender sender, String[] args) {
		// TODO Auto-generated method stub
		
	}



	private void commands_items_add(CommandSender sender, String[] args) {
		if(sender instanceof Player){
			Player player = (Player) sender;
			ItemStack item = player.getItemInHand();
			if(Settings.items.contains(item)){
				Msg.sendMsg(sender, "This item is already in the list.");
				return;
			}
			Settings.items.add(item);
			Msg.sendMsg(player, "Item was added");
			Settings.saveSettings();
		}else{
			Msg.sendMsg(sender, "Only players can run this command");
		}
		
	}



	private void commands_items_help(CommandSender sender, String[] args) {
		// TODO Auto-generated method stub
		
	}



	private void command_cancel(CommandSender sender, String[] args) {
		if(this.task == null){
			Msg.sendMsg(sender, "FinderChest Task isnt running.");
			return;
		}
		if(this.task.isFinished()){
			Msg.sendMsg(sender, "FinderChest Task isnt running.");
			return;
		}else{
			this.task.cancel();
			Msg.sendMsg(sender, "FinderChest Task was cancelled.");
		}
		
	}



	private void command_next(CommandSender sender, String[] args) {
		// TODO Auto-generated method stub
		
	}



	private void command_scanchest(CommandSender sender, String[] args) {
		if(args.length<2){
			sender.sendMessage("Please use /chestcheck scan <worldname>");
			return;
		}
		
		World w = Bukkit.getServer().getWorld(args[1]);
		
		if(w== null){
			sender.sendMessage("Invalid World");
			return;
		}
		sender.sendMessage("Iniciando busqueda de chest");
		
		this.task = new FinderTask(w,sender);
		Bukkit.getScheduler().runTaskAsynchronously(ChestFinder.plugin, this.task);
		
		sender.sendMessage("Busqueda de chest iniciada");
	}

	
	

/*	private void command_scan(CommandSender sender, String[] args) {

		if(args.length<2){
			Msg.sendMsg(sender, "Please use /chestcheck scan <worldname>");
			return;
		}
		World w = Bukkit.getServer().getWorld(args[1]);
		
		if(w== null){
			Msg.sendMsg(sender, "Invalid World");
			return;
		}
		
		File f = new File(ChestFinder.plugin.getDataFolder(), w.getName()+".yml");
		
		if(!f.exists()){
			Msg.sendMsg(sender, "First Find Chest in "+w.getName()+" with /chestcheck findchest "+w.getName()+" before scan chest.");
		}
		
		YamlConfiguration cfg = UtilChestFinder.loadconfig(w.getName()+".yml");
		List<Location> chestscantrue = new ArrayList<Location>();
		
		for(String key : cfg.getKeys(false)){
			
			String locunparsed = cfg.getString(key);
			
			String[] part = locunparsed.split(",");
			
			if(part.length<3) continue;
			
			Location loc = new Location(w,Integer.valueOf(part[0]),Integer.valueOf(part[1]),Integer.valueOf(part[2]));
			
			if(loc==null) continue;
			
			if(loc.getBlock().getState() instanceof Chest){
			     Chest chest = (Chest) loc.getBlock().getState();
			     Inventory inv = chest.getInventory();
			     
			     for(ItemStack it : Settings.items)
				        if(inv.containsAtLeast(it,1)){
				        	chestscantrue.add(loc);
				        	continue;
				        }
			}
			
			
		}
		
		
		PrintWriter writer;
		try {
			writer = new PrintWriter("FindChestResult.txt", "UTF-8");
			for(Location l : chestscantrue){
				writer.println("- "+l.getBlockX()+" , "+l.getBlockY()+" , " +l.getBlockZ());
			}
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Msg.sendMsg(sender, "Found "+chestscantrue.size()+" Chest with items settings.");
		Msg.sendMsg(sender, "See FindChestResult.txt in your server forlder.");


	}
*/

	private void command_reload(CommandSender sender, String[] args) {
		Settings.loadSettings();
		sender.sendMessage("Configuracion recargada");
	}

	private void command_help(CommandSender sender, String[] args) {
		sender.sendMessage("/chestcheck additem  - Agrega el item de tu mano a la lista de busqueda");
		sender.sendMessage("/chestcheck help - Muestra la ayuda");
		sender.sendMessage("/chestcheck findchest world - Inicia busqueda de chest");
		sender.sendMessage("/chestcheck scan world - Scanea los cofres");
		sender.sendMessage("/chestcheck reload - Recarga la config");
	}
}
