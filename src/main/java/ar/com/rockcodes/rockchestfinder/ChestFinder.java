package ar.com.rockcodes.rockchestfinder;

import org.bukkit.plugin.java.JavaPlugin;

import ar.com.rockcodes.rockchestfinder.commands.Commands;

public class ChestFinder extends JavaPlugin{

	public static ChestFinder plugin ;
	
	@Override
	public void onDisable() {
		Settings.saveSettings();
	}

	@Override
	public void onEnable() {
		
		ChestFinder.plugin = this;
		this.saveDefaultConfig();
		Settings.loadSettings();
		getCommand("chestfinder").setExecutor(new Commands());
		
	}

}
